const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const app = express();
const db = require('./config/keys').mongoURI;
const io = require('socket.io').listen(app.listen(5000), {origins: '*:*'});


// bodyparser
app.use(express.urlencoded({extended: false}));
app.use(bodyparser.json())

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// io.origins('*:*');

app.use(function(req, res, next) {
    req.io = io;
    next();
})

// // connect to Mongo
mongoose.connect(db, { useNewUrlParser: true })
    .then(()=> console.log('MongoDB connected...'))
    .catch(err => console.log(err));


// Routes
app.use('/api', require('./routes/index/index.routes'));

const PORT = process.env.PORT || 5000;

// const server = app.listen(PORT, console.log(`Server started on port... ${PORT}`));
console.log(`Server started on port... ${PORT}`)
// const io = socketio(server);
// require('./routes/index/index.routes')(app, io);
io.set('origins', '*:*');

module.exports = app

// const db = require('./services/mongodb.service');

