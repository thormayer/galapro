const express = require('express');
const router = express.Router();
const logic = require('./index.logic');




router.post('/', (req, res) => {
    
    const url = req.body.url;
    req.io.sockets.emit('update', url); 
    if(logic.isValidUrl(url)) {
        logic.saveUrl(url).then((response, reject)=> {
            res.status(200).json(response)
        })
    } else{
        res.status(200).json({code : 2,  message: 'url is not valid. are you trying something shady here??'})
    }

})

module.exports = router;