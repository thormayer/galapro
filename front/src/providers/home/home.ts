import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HomeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HomeProvider {
  

  baseUrl = `http://localhost:5000/api`

  constructor(public http: HttpClient) {
    console.log('Hello HomeProvider Provider');
  }

  setNewURL(url) {
    return this.http.post(`${this.baseUrl}/`,  {url});
  }
}
