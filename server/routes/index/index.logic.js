const Redirects = require('../../models/Redirects');

const logic = {
    saveUrl: async function (path) {
        let response;
        let urlArr = await this.findUrl(path);
        if (this.isURLExists(urlArr))
            response = { code: 2, message: 'Sorry, URL already Exists...' }
        else {
            this.saveToMongo(path);
            response = { code: 1, message: 'URL Saved!' }
        }
        return await response;
    },
    findUrl: async function (path) {
        return await Redirects.find({ 'url': path });
    },
    isURLExists: function (results) {
        return results.length > 0 ? true : false;
    },
    saveToMongo: function (url) {
        let newUrl = new Redirects({ url: url });
        newUrl.save().then(path => {
            console.log(`${path} saved!`)
        })
    },
    isValidUrl: function (value) {
        return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
    }
}

module.exports = logic;