const mongoose = require('mongoose');
const RedirectSchema = new mongoose.Schema({
    url: {
        type: String,
        required: true
    },
})


const Redirect = mongoose.model('Redirect', RedirectSchema);
module.exports = Redirect;